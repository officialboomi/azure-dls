# Azure Data Lake Gen2 Connector #
Azure Data Lake Gen2 Connector enables you to connect to the Azure Data lake Gen2 storage account using Azure Data Lake Storage Gen2 Rest API. Users can use this connector to create,list,delete the filesystem and paths ,get and update the paths via Azure Data Lake Gen2 Rest API Services. 



### Connector configuration ###
To configure the connector to interact with Azure Data lake Gen2 storage account, please set up two components:

*	Azure Data Lake Gen2 connection � The Azure Data Lake Gen2 connector uses OAuth 2.0 authentication for authenticating users in a more secure way to access the resources in storage account.



*   Azure Data Lake Gen2 operation � The operation represents an action against Azure Data Lake Gen2 storage account. You will create one or more operations, one for each type of interaction required by your integration scenario.

This design provides reusable components containing connection settings and operation settings. After building your connection and operation, set up your connector within a process. When the process is defined properly, Boomi Integration send Rest API requests to  Azure Data Lake Gen2 Rest API using the Azure Data Lake Gen2 connector to perform operation.



### Prerequisites ###
*	Register the app in Azure Active Directory and generate secret key for the same.Add the role assignment(Owner,Reader,Storage Blob Data Contributor) for the registered app in the Azure Data Lake Gen2 storage account.



*	API Integrations with an Access Token: 
	For REST API integrations that use access tokens for authorization, locate the Authentication Base URIs on the Endpoint component of registered app created in App registration.

### Tracked properties ###
This connector has no tracked properties.

### Compiling ###
Once your JAVA_HOME points to your installation of JDK 1.8 (or above) and JAVA_HOME/bin and Apache maven are in your PATH, issue the following command to build the jar file:

	mvn install

or if you don't want to run the unit tests, then run

	mvn install -DskipTests
 
### Running the unit tests ###
To run the unit tests, please use below command

	mvn install 
  	
It will run the unit tests and build the jar file. Both the CAR file and the Test Reports will be available inside target folder.

### Supported Operations ###
The Azure Data Lake Gen2 operations define how to interact with your Azure Data Lake  Storage Gen2 account and represent a specific action (Create, Update, Delete, Get, and List Filesystem/Paths). Create a separate operation component for each action/object combination that your integration requires. 

### Azure Data Lake Gen2 connection ###
The Azure Data Lake Gen2 connector uses OAuth 2.0 authentication for authenticating users in a more secure way to login to the portal.

### Connection Tab ###
##### Storage account #####
Enter the Azure Data Lake Gen2 Storage account name.

##### Response Timeout #####
Enter the operation timeout(in milliseconds).

##### Client ID #####
Enter the client ID of Registered App in Azure portal that is required to generate a valid access token

##### Client Secret #####
Enter the client secret of Registered App in Azure portal Cloud for the client id. 

##### Access Token URL #####
The URL of the endpoint that provides the access tokens.





### Azure Data Lake Gen2 operation ###
The Azure Data Lake Gen2 Connector operations support the following actions:

*	Create: Creates a new FileSystem/Path in the Azure Data Lake Gen2 Storage Account. 
*	Update: Updates the content of the file in the given Path of Azure Data Lake Gen2 Storage Account.
*	Delete: Deletes the FileSystem/Path in the Azure Data Lake Gen2 Storage Account.
*	List:  List the FileSystem/Path in the Azure Data Lake Gen2 Storage Account.
*	Get : Gets the content of the file in the given Path of Azure Data Lake Gen2 Storage Account.

### Operation Details ###

### Create FileSystem###
The CREATE Filesytem operation helps you to create a FileSystem through the Rest API using an create request. User have to provide the unique filesystem name that has to created from Azure Data Lake Storage Gen2 Rest API.

### List FileSystem###
The List Filesystem operation helps you to list a filesystems & their properties through the Rest API using an get request. Users can provide the prefix value to lit the filesystems. 

### Delete FileSystem ###
The DELETE FileSystem operation helps you to delete a FileSystem through the Rest API using the delete request. User have to provide the filesystem name that has to deleted from Azure Data Lake Storage Gen2 Rest API.

### Create Path###
The CREATE Path operation helps you to create a path through the Rest API using an create request. User have to provide the directory/filename name that has to created from Azure Data Lake Storage Gen2 Rest API.

### Update Path###
The UPDATE Path operation helps you to update the content of the file through the Rest API using an patch request. Users have to provide the filename and input to be updated. 

### Delete Path ###
The DELETE Path operation helps you to delete a path through the Rest API using the delete request. User have to provide the path(directory/filename) that has to deleted from Azure Data Lake Storage Gen2 Rest API.

### Get Path###
The GET Path operation helps you to read the content of the file through the Rest API using an get request. Users have to provide the filename to be read from Azure Data Lake Storage Gen2 Rest API. 


## Additional resources ##
* [Azure Data Lake Storage Gen2 REST API](https://docs.microsoft.com/en-us/rest/api/storageservices/data-lake-storage-gen2)
* [Azure Data Lake Storage Account](https://docs.microsoft.com/en-us/azure/storage/blobs/create-data-lake-storage-account)