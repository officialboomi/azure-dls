//Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake.utils;

import com.boomi.util.StringUtil;

/**
 * The Class UniqueNamesGenerator.
 *
 *
 */
public class UniqueNamesGenerator {

	/**
	 * Instantiates a new unique names generator.
	 */
	private UniqueNamesGenerator() {

	}

	/**
	 * Generate unique name.
	 *
	 * @param prefix the prefix
	 * @return the string
	 */
	public static String generateUniqueName(String prefix) {
		return prefix + System.currentTimeMillis();
	}

	/**
	 * Generate unique file name.
	 *
	 * @param fileName the file name
	 * @return the string
	 */
	public static String generateUniqueFileName(String fileName) {
		String nameNoExt = getNameWithoutExtension(fileName);
		String fileExt = getFileExtension(fileName);
		String newName = generateUniqueName(nameNoExt);
		if (!StringUtil.isBlank(fileExt)) {
			newName = newName + "." + fileExt;
		}
		return newName;
	}

	/**
	 * Gets the name without extension.
	 *
	 * @param fileName the file name
	 * @return the name without extension
	 */
	private static String getNameWithoutExtension(String fileName) {
		int lastDot = fileName.lastIndexOf('.');
		if (lastDot < 0) {
			return fileName;
		}
		return fileName.substring(0, lastDot);
	}

	/**
	 * Gets the file extension.
	 *
	 * @param fileName the file name
	 * @return the file extension
	 */
	private static String getFileExtension(String fileName) {
		int lastDot = fileName.lastIndexOf('.');
		if (lastDot < 0) {
			return "";
		}
		return fileName.substring(lastDot + 1);
	}

}
