// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake.utils;

/**
 * The Class Constants.
 *
 */
public class Constants {

	/** The Constant ENDPOINT_KEY. */
	public static final String ENDPOINT_KEY = "endpoint";

	/** The Constant TENANT_ID_KEY. */
	public static final String TENANT_ID_KEY = "tenant_id";

	/** The Constant CLIENT_ID_KEY. */
	public static final String CLIENT_ID_KEY = "client_id";

	/** The Constant PREFIX. */
	public static final String PREFIX = "prefix";

	/** The Constant DIRECTORY. */
	public static final String DIRECTORY = "directory";

	/** The Constant FILE. */
	public static final String FILE = "file";

	/** The Constant MAX_RESULT. */
	public static final String MAX_RESULT = "maxResults";

	/** The Constant CLIENT_SECRET_KEY. */
	public static final String CLIENT_SECRET_KEY = "client_secret";

	/** The Constant SCOPE_KEY. */
	public static final String SCOPE_KEY = "scope";

	/** The Constant RECURSIVE. */
	public static final String RECURSIVE = "recursive";

	/** The Constant GRANT_TYPE_KEY. */
	public static final String GRANT_TYPE_KEY = "grant_type";

	/** The Constant RESPONSE_TIMEOUT. */
	public static final String RESPONSE_TIMEOUT = "timeout";

	/** The Constant ACTION_IF_FILE_EXISTS. */
	public static final String ACTION_IF_FILE_EXISTS = "actionIfFileExists";

	/** The Constant GRANT_TYPE_VALUE. */
	public static final String GRANT_TYPE_VALUE = "client_credentials";

	/** The Constant CONTENT_TYPE_KEY. */
	public static final String CONTENT_TYPE_KEY = "Content-Type";

	/** The Constant CONTENT_TYPE_VALUE. */
	public static final String CONTENT_TYPE_VALUE = "application/x-www-form-urlencoded";

	/** The Constant ACCESS_TOKEN_KEY. */
	public static final String ACCESS_TOKEN_KEY = "access_token";

	/** The Constant EXPIRES_IN_KEY. */
	public static final String EXPIRES_IN_KEY = "expires_in";

	/** The Constant ACCOUNT_NAME. */
	public static final String ACCOUNT_NAME = "storage_account";

	/** The Constant TOKEN_URL. */
	public static final String TOKEN_URL = "token_url";

	/** The Constant RESOURCE. */
	public static final String RESOURCE = "resource";

	/** The Constant LIST. */
	public static final String LIST = "LIST";

	/** The Constant DELETE. */
	public static final String DELETE = "DELETE";

	/** The Constant GET. */
	public static final String GET = "GET";

	/** The Constant GET. */
	public static final String CREATE = "CREATE";
	/** The Constant FILE_SYSTEM. */
	public static final String FILE_NAME = "filename";

	/** The Constant FILE_SYSTEM. */
	public static final String FILE_SYSTEM = "filesystem";

	/** The Constant PATH. */
	public static final String PATH = "Path";

	/** The Constant ACCOUNT. */
	public static final String ACCOUNT = "account";

	/** The Constant X_MS_VERSION. */
	public static final String X_MS_VERSION = "x-ms-version";

	/** The Constant X_MS_VERSION_ID. */
	public static final String X_MS_VERSION_ID = "2020-10-02";

	/** The Constant X_MS_CONTINUATION. */
	public static final String X_MS_CONTINUATION = "x-ms-continuation";

	/** The Constant CONTINUATION. */
	public static final String CONTINUATION = "continuation";

	/** The Constant AUTHORIZATION. */
	public static final String AUTHORIZATION = "Authorization";

	/** The Constant BEARER. */
	public static final String BEARER = "Bearer";

	/** The Constant DELETE_TYPE. */
	public static final String DELETE_TYPE = "DeleteType";

	/** The Constant ERROR_DESCRIPTION. */
	public static final String ERROR_DESCRIPTION = "error_description";

	/** The Constant MESSAGE. */
	public static final String MESSAGE = "message";

	/** The Constant FILESYSTEM. */
	public static final String FILESYSTEM = "FileSystem";
	/** The Constant JSON_DRAFT4_DEFINITION. */
	public static final String JSON_DRAFT4_DEFINITION = "\n\"$schema\": \"http://json-schema.org/draft-07/schema#\",";

	/** The Constant LOG_ACCESS_TOKEN_EXPIRY. */
	public static final String LOG_ACCESS_TOKEN_EXPIRY = "AccessToken expired";

	/** The Constant FORCE_UNIQUE_NAMES. */
	public static final String FORCE_UNIQUE_NAMES = "FORCE_UNIQUE_NAMES";

	/** The Constant FILESYSTEM_INPUT_MSG. */
	public static final String FILESYSTEM_INPUT_MSG = "File System input is missing, please provide input for File System";

	/** The Constant FILE_INPUT_MSG. */
	public static final String FILE_INPUT_MSG = "File Name is missing, please provide input for File Name";

	/** The Constant INPUT_MSG. */
	public static final String INPUT_MSG = "Input is missing, please provide input for File Name or Directory to be deleted";

	/** The Constant DELETE_SUCESS_MSG. */
	public static final String DELETE_SUCESS_MSG = "Deleted Successfully";

	/** The Constant IF_MODIFIED_SINCE. */
	public static final String IF_MODIFIED_SINCE = "If-Modified-Since";

	/** The Constant IF_UNMODIFIED_SINCE. */
	public static final String IF_UNMODIFIED_SINCE = "If-Unmodified-Since";

	/** The Constant TARGET_DIR. */
	public static final String TARGET_DIR = "targetDir";

	/** The Constant ACTION. */
	public static final String ACTION = "action";

	/** The Constant STATUS. */
	public static final String STATUS = "getStatus";

	/** The Constant CONTENT_LENGTH. */
	public static final String CONTENT_LENGTH = "Content-Length";

	/** The Constant APPEND. */
	public static final String APPEND = "append";

	/** The Constant FLUSH. */
	public static final String FLUSH = "flush";

	/** The Constant POSITION. */
	public static final String POSITION = "position";

	/** The Constant CONTENT. */
	public static final String CONTENT = "Content-Type";

	/** The Constant OCTET_STREAM. */
	public static final String OCTET_STREAM = "application/octet-stream";

	/** The Constant INPUT_MSG. */
	public static final String UPDATE_INPUT_MSG = "Please provide input to update the file";

	/** The Constant INPUT_MSG. */
	public static final String PATH_MSG = "The Specified Filesystem Path does not exist";

	/** The Constant UPDATE_SUCCESS_MSG. */
	public static final String UPDATE_SUCCESS_MSG = "Updated Successfully";

	/** The Constant CREATE_TYPE. */
	public static final String CREATE_TYPE = "CreateType";

	/** The Constant LIST_TYPE. */
	public static final String LIST_TYPE = "ListType";

	/** The Constant DATE_ERROR_MSG. */
	public static final String DATE_ERROR_MSG = "Please provide the date in MM/dd/yyyy, h:mm:ss a format.";

	/** The Constant CREATE_SUCCESS_MSG. */
	public static final String CREATE_SUCCESS_MSG = "Created Successfully";

	/** The Constant EMPTY_CREATE_SUCCESS_MSG. */
	public static final String EMPTY_CREATE_SUCCESS_MSG = "Empty File Created Successfully";

	/** The Constant CHUNK_SIZE. */
	public static final long CHUNK_SIZE = 1L * 1024 * 1024;

	/** The Constant ERROR_PROCESSING_BODY_CONTENT. */
	public static final String ERROR_PROCESSING_BODY_CONTENT = "couldn't process response body";

}
