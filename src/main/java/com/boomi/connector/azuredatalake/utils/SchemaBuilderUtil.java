// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake.utils;

import java.io.IOException;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.azuredatalake.model.CreateResponse;
import com.boomi.connector.azuredatalake.model.ListFileSystem;
import com.boomi.connector.azuredatalake.model.ListPathResponse;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;

/**
 * The Class SchemaBuilderUtil.
 * 
 */
public class SchemaBuilderUtil {
	/**
	 * Instantiates a new schema builder util.
	 */
	private SchemaBuilderUtil() {

	}

	/**
	 * Gets the input json schema.
	 *
	 * @param opsType the ops type
	 * @param subType the sub type
	 * @return the input json schema
	 */
	public static String getInputJsonSchema(String opsType, String subType) {
		String jsonSchema = null;
		StringBuilder sbSchema = new StringBuilder();
		String json = null;
		sbSchema.append("{").append(Constants.JSON_DRAFT4_DEFINITION).append(" \"").append(JSONUtil.SCHEMA_TYPE)
		.append("\": \"object\",").append(" \"").append(JSONUtil.SCHEMA_PROPERTIES).append("\": {")
		.append("\"");
		if ((opsType.equals(Constants.DELETE) || opsType.equals("CREATE")) && subType.equals(Constants.FILESYSTEM)) {
			sbSchema.append(Constants.FILESYSTEM).append("\": {").append(" \"").append(JSONUtil.SCHEMA_TYPE)
			.append("\": \"").append("string").append("\"").append("}}}");
		}
		if ((opsType.equals(Constants.DELETE) || opsType.equals(Constants.GET)) && subType.equals(Constants.PATH)) {
			sbSchema.append(Constants.DIRECTORY).append("\": {").append(" \"").append(JSONUtil.SCHEMA_TYPE)
			.append("\": \"").append("string").append("\"").append("},").append("\"")
			.append(Constants.FILE_NAME).append("\": {").append(" \"").append(JSONUtil.SCHEMA_TYPE)
			.append("\": \"").append("string").append("\"").append("}}}");
		}
		if (opsType.equals(Constants.LIST)) {
			if (subType.equals(Constants.PATH)) {
				sbSchema.append(Constants.DIRECTORY).append("\": {").append(" \"").append(JSONUtil.SCHEMA_TYPE)
				.append("\": \"").append("string").append("\"").append("}}}");
			} else {
				sbSchema.append(Constants.PREFIX).append("\": {").append(" \"").append(JSONUtil.SCHEMA_TYPE)
				.append("\": \"").append("string").append("\"").append("}}}");
			}
		}
		json = sbSchema.toString();
		JsonNode rootNode = null;

		try {

			rootNode = JSONUtil.getDefaultObjectMapper().readTree(json);
			if (rootNode != null) {
				jsonSchema = JSONUtil.prettyPrintJSON(rootNode);
			}
		} catch (Exception e) {
			throw new ConnectorException(e);
		}

		return jsonSchema;

	}

	public static String getJsonSchema(String subType, String opsType) {

		ObjectMapper mapper = JSONUtil.getDefaultObjectMapper().disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS)
				.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		String json = null;
		try {
			SchemaFactoryWrapper wrapper = new SchemaFactoryWrapper();
			if (opsType.equals("CREATE") || opsType.equals("DELETE")) {
				mapper.acceptJsonFormatVisitor(CreateResponse.class, wrapper);
			}
			if (opsType.equals("LIST")) {
				if (subType.equals("Path")) {
					mapper.acceptJsonFormatVisitor(ListPathResponse.class, wrapper);
				} else {
					mapper.acceptJsonFormatVisitor(ListFileSystem.class, wrapper);
				}
			} else {
				mapper.acceptJsonFormatVisitor(CreateResponse.class, wrapper);
			}
			JsonSchema schema = wrapper.finalSchema();
			json = JSONUtil.prettyPrintJSON(schema);
		} catch (Exception e) {
			throw new ConnectorException("Failed to build Schema", e);
		}

		return json;
	}

	public static String getOperationType(String cookie, String type) {
		String operationType = null;
		if (cookie != null && !cookie.isEmpty()) {
			JsonNode json;
			try {
				json = JSONUtil.getDefaultObjectMapper().readTree(cookie);
				JsonNode cookieMap = json.get(type);
				if (cookieMap != null)
					operationType = cookieMap.asText();
			} catch (IOException e) {
				throw new ConnectorException(e);
			}

		}
		return operationType;

	}

}
