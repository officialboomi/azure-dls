// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake.utils;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.azuredatalake.model.ErrorResponse;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

/**
 * The Class CustomResponseUtil.
 * 
 */
public class CustomResponseUtil {

	/**
	 * Instantiates a new custom response util.
	 */
	private CustomResponseUtil() {

	}

	/**
	 * Write error response.
	 *
	 * @param objdata      the objdata
	 * @param response     the response
	 * @param httpResponse the http response
	 * @param responseUtil the response util
	 */
	public static void writeHttpErrorResponse(ObjectData objdata, OperationResponse response,
			CloseableHttpResponse httpResponse) {
		JsonParser parser = null;
		InputStream inputStream = null;
		String errormsg = null;
		Payload responseUtil = null;
		HttpEntity entity = httpResponse.getEntity();
		try {
			if (entity != null) {
				inputStream = entity.getContent();
				parser = JSONUtil.getDefaultObjectMapper().getFactory().createParser(inputStream);
				errormsg = getErrorMessage(parser, Constants.MESSAGE);
				responseUtil = JsonPayloadUtil
						.toPayload(new ErrorResponse(httpResponse.getStatusLine().getStatusCode(), errormsg));
			} else {
				responseUtil = JsonPayloadUtil.toPayload(new ErrorResponse(httpResponse.getStatusLine().getStatusCode(),
						httpResponse.getStatusLine().getReasonPhrase()));
			}
			response.addResult(objdata, OperationStatus.APPLICATION_ERROR,
					StringUtil.toString(httpResponse.getStatusLine().getStatusCode()), errormsg, responseUtil);
		} catch (UnsupportedOperationException | IOException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(inputStream, parser, responseUtil, httpResponse);
		}

	}

	/**
	 * Write json error response.
	 *
	 * @param e        the e
	 * @param response the response
	 * @param objdata  the objdata
	 */
	public static void writeExceptionErrorResponse(Exception e, OperationResponse response, ObjectData objdata) {
		if (e.getMessage() != null || !e.getMessage().isEmpty()) {
			try (Payload payload = JsonPayloadUtil.toPayload(new ErrorResponse(405, e.getMessage()))) {
				response.addResult(objdata, OperationStatus.APPLICATION_ERROR, StringUtil.toString(405), e.getMessage(),
						payload);
			} catch (Exception ge) {
				throw new ConnectorException(ge);
			}
		} else {
			try (Payload payload = JsonPayloadUtil.toPayload(new ErrorResponse(400, Constants.FILESYSTEM_INPUT_MSG))) {
				response.addResult(objdata, OperationStatus.APPLICATION_ERROR, StringUtil.toString(400),
						Constants.FILESYSTEM_INPUT_MSG, payload);
			} catch (IOException ioE) {
				throw new ConnectorException(ioE);
			}
		}
	}

	/**
	 * Gets the error message.
	 *
	 * @param jParser   the j parser
	 * @param jsonField the json field
	 * @return the error message
	 */
	public static String getErrorMessage(JsonParser jParser, String jsonField) {
		String errorMsg = null;
		try {
			while (jParser.nextToken() != JsonToken.END_OBJECT) {
				String fieldname = jParser.getCurrentName();
				if (jsonField.equals(fieldname)) {
					jParser.nextToken();
					errorMsg = jParser.getText();
					break;
				}
			}
		} catch (IOException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(jParser);
		}
		return errorMsg;
	}

	/**
	 * Write application error response.
	 *
	 * @param objdata  the objdata
	 * @param response the response
	 * @param message  the message
	 */
	public static void writeApplicationErrorResponse(ObjectData objdata, OperationResponse response, String message) {
		try (Payload payload = JsonPayloadUtil.toPayload(new ErrorResponse(400, message))) {
			response.addResult(objdata, OperationStatus.APPLICATION_ERROR, StringUtil.toString(400), message, payload);

		} catch (Exception e) {
			throw new ConnectorException(e);
		}

	}
}
