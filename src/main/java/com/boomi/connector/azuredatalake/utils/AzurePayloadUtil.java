// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake.utils;

import java.io.IOException;
import java.io.OutputStream;

import com.boomi.connector.api.BasePayload;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

public class AzurePayloadUtil extends BasePayload {

	/**
	 * It takes JSON Parser output object , write into an OutputStream and return
	 * the BasePayload object
	 * 
	 * @param parser
	 * @return {@link BasePayload}
	 * 
	 */

	public static BasePayload toPayload(final JsonParser parser) {
		if (parser == null) {
			return null;
		}

		return new BasePayload() {
			@Override
			public void writeTo(OutputStream out) throws IOException {
				JsonFactory factory = JSONUtil.getDefaultObjectMapper().getFactory();
				try (JsonGenerator generator = factory.createGenerator(out)){
					generator.copyCurrentStructure(parser);
					generator.flush();
				} 
			}
		};

	}

}
