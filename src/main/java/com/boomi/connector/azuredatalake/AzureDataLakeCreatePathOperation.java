// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.azuredatalake.model.CreateResponse;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.CustomResponseUtil;
import com.boomi.connector.azuredatalake.utils.UniqueNamesGenerator;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class AzureDataLakeCreatePathOperation extends BaseUpdateOperation {

	private AzureDataLakeConnection conn = null;

	protected AzureDataLakeCreatePathOperation(AzureDataLakeConnection conn) {
		super(conn);
		this.conn = conn;
	}

	/**
	 * Execute update.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		for (ObjectData data : request) {
			String directoryName = data.getDynamicProperties().get(Constants.TARGET_DIR);
			String fileName = data.getDynamicProperties().get(Constants.FILE_NAME);
			String fileSystemName = getContext().getObjectTypeId();
			String url = this.conn.getUrl() + "/" + fileSystemName;
			String fileActionType = getContext().getOperationProperties().getProperty(Constants.ACTION_IF_FILE_EXISTS);
			if (StringUtil.isEmpty(directoryName))
				directoryName = getContext().getOperationProperties().getProperty(Constants.TARGET_DIR, "");

			if (!StringUtil.isEmpty(directoryName)) {
				url = url + "/" + directoryName;
			}

			if (fileName != null) {
				try {
					if (fileActionType.equals(Constants.FORCE_UNIQUE_NAMES)) {
						boolean fileExist = getProperties(url, fileName);
						if (fileExist)
							fileName = UniqueNamesGenerator.generateUniqueFileName(fileName);
					}
					url = url + "/" + fileName;

					createFile(data, url, response, fileName, fileActionType);
				} catch (IOException | URISyntaxException e) {
					CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
				}
			} else {
				CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.FILE_INPUT_MSG);

			}

		}
	}

	/**
	 * Creates the file.
	 *
	 * @param data           the data
	 * @param url            the url
	 * @param response       the response
	 * @param fileName       the file name
	 * @param fileActionType the file action type
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void createFile(ObjectData data, String url, OperationResponse response, String fileName,
			String fileActionType) throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

			URIBuilder builder = new URIBuilder(url);
			builder.setParameter(Constants.RESOURCE, Constants.FILE);
			HttpPut httpPut = new HttpPut(builder.build().toString());
			httpPut.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
			httpPut.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());
			if ("ERROR".equals(fileActionType)) {
				httpPut.addHeader("If-None-Match", "*");
			}
			httpResponse = httpClient.execute(httpPut);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_CREATED) {
				if (data.getDataSize() != 0)
					uploadData(data, url, response, fileName, httpClient);
				else {
					try (Payload payload = JsonPayloadUtil
							.toPayload(new CreateResponse("", fileName, Constants.EMPTY_CREATE_SUCCESS_MSG))) {
						ResponseUtil.addSuccess(response, data, StringUtil.toString(statusCode), payload);
					}

				}
			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}
		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
	}

	/**
	 * Upload data.
	 *
	 * @param data       the data
	 * @param url        the url
	 * @param response   the response
	 * @param fileName   the file name
	 * @param httpClient the http client
	 * @throws IOException        Signals that an I/O exception has occurred.
	 * @throws URISyntaxException the URI syntax exception
	 */
	public void uploadData(ObjectData data, String url, OperationResponse response, String fileName,
			CloseableHttpClient httpClient) throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		long inputLength = 0L;
		try (InputStream input = data.getData()) {
			byte[] buf = new byte[(int) Constants.CHUNK_SIZE];
			int length;
			while ((length = input.read(buf)) > 0) {
				byte[] buf2 = new byte[length];
				// The byte array is loaded with 1MB of the data at a time and it is not causing
				// memory issue here.
				System.arraycopy(buf, 0, buf2, 0, length);
				this.invokeUpdate(url, Constants.APPEND, inputLength, buf2, httpClient);
				inputLength += buf2.length;
			}
			httpResponse = this.invokeUpdate(url, Constants.FLUSH, inputLength, null, httpClient);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode / 100 == 2) {
				try (Payload payload = JsonPayloadUtil
						.toPayload(new CreateResponse("", fileName, Constants.CREATE_SUCCESS_MSG))) {
					ResponseUtil.addSuccess(response, data, StringUtil.toString(statusCode), payload);
				}
			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}
		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
	}

	/**
	 * Gets the properties.
	 *
	 * @param url      the url
	 * @param fileName the file name
	 * @return the properties
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public boolean getProperties(String url, String fileName) throws IOException, URISyntaxException {
		boolean fileExist = false;
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			url = url + "/" + fileName;
			URIBuilder builder = new URIBuilder(url);
			builder.setParameter(Constants.ACTION, Constants.STATUS);
			HttpHead httpHead = new HttpHead(builder.build().toString());
			httpHead.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
			httpHead.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());

			httpResponse = httpClient.execute(httpHead);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				fileExist = true;
			}
			return fileExist;
		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
	}

	/**
	 * Invoke update. calling append api with 1MB of data at a time
	 * 
	 * @param path   the path
	 * @param action the action
	 * @param size   the size
	 * @param data   the data
	 * @return the closeable http response
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public CloseableHttpResponse invokeUpdate(String path, String action, long size, byte[] buf,
			CloseableHttpClient httpClient) throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		try {
			URIBuilder builder2 = new URIBuilder(path);
			builder2.setParameter("action", action);
			builder2.setParameter("position", String.valueOf(size));
			builder2.setParameter(Constants.RESPONSE_TIMEOUT, this.conn.getTimevalue());
			HttpPatch httpPatch = new HttpPatch(builder2.build().toString());
			httpPatch.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
			httpPatch.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());
			httpPatch.addHeader(Constants.CONTENT, Constants.OCTET_STREAM);
			if ("append".equals(action)) {
				httpPatch.setEntity(new ByteArrayEntity(buf));
			}

			httpResponse = httpClient.execute(httpPatch);

			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode / 100 != 2) {
				throw new ConnectorException(httpResponse.getStatusLine().getReasonPhrase());
			}

		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
		return httpResponse;
	}
}
