// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.azuredatalake.model.CreateResponse;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.CustomResponseUtil;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

/**
 * The Class AzureDataLakeUpdateOperation.
 *
 */
public class AzureDataLakeUpdateOperation extends BaseUpdateOperation {

	AzureDataLakeConnection conn = null;

	/**
	 * Instantiates a new azure data lake update operation.
	 *
	 * @param conn the conn
	 */
	public AzureDataLakeUpdateOperation(AzureDataLakeConnection conn) {
		super(conn);
		this.conn = conn;
	}

	/**
	 * Execute update.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		for (ObjectData data : request) {
			String directoryName = data.getDynamicProperties().get(Constants.TARGET_DIR);
			String fileName = data.getDynamicProperties().get(Constants.FILE_NAME);
			StringBuilder url = new StringBuilder();
			StringBuilder path = new StringBuilder();
			if (StringUtil.isEmpty(directoryName))
				directoryName = getContext().getOperationProperties().getProperty(Constants.TARGET_DIR, "");
			url.append(this.conn.getUrl()).append("/").append(getContext().getObjectTypeId());
			if (!StringUtil.isEmpty(directoryName)) {
				url.append("/").append(directoryName);
				path.append(directoryName);
			}
			if (!StringUtil.isEmpty(fileName)) {
				url.append("/").append(fileName);
				path.append("/").append(fileName);
				try {
					uploadData(data, url.toString(), response, path.toString());
				} catch (IOException | URISyntaxException e) {
					CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
				}
			} else {
				CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.FILE_INPUT_MSG);
			}
		}
	}

	/**
	 * Gets the properties.
	 *
	 * @param url the url
	 * @return the properties size
	 * @throws URISyntaxException , IOException 
	 */
	private long getProperties(String url) throws URISyntaxException , IOException {
		long value = -1;
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			URIBuilder builder = new URIBuilder(url);
			builder.setParameter(Constants.ACTION, Constants.STATUS);
			HttpHead httpHead = new HttpHead(builder.build().toString());
			httpHead.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
			httpHead.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());
			httpResponse = httpClient.execute(httpHead);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				Header[] headers = httpResponse.getHeaders(Constants.CONTENT_LENGTH);
				for (Header header : headers) {
					if (!header.getValue().isEmpty()) {
						value = Long.parseLong(header.getValue());
					}
				}
			}
		}finally {
			IOUtil.closeQuietly(httpResponse);
		}
		return value;
	}

	/**
	 * Upload data.
	 *
	 * @param data     the data
	 * @param url      the url
	 * @param response the response
	 * @param path     the path
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	private void uploadData(ObjectData data, String url, OperationResponse response, String path) throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		long totalSize = this.getProperties(url);
		if (totalSize >= 0) {
			try (InputStream input = data.getData()) {
				if (input.available() != 0) {
					byte[] buf = new byte[(int) Constants.CHUNK_SIZE];
					int length = 0;
					while ((length = input.read(buf)) > 0) {
						byte[] buf2 = new byte[length];
						// The byte array is loaded with 1MB of the data at a time and it is not causing
						// memory issue here.
						System.arraycopy(buf, 0, buf2, 0, length);
						this.invokeUpdate(url, Constants.APPEND, totalSize, buf2);
						totalSize += buf2.length;
					}
					httpResponse = this.invokeUpdate(url, Constants.FLUSH, totalSize, null);
					int statusCode = httpResponse.getStatusLine().getStatusCode();
					if (statusCode / 100 == 2) {
						try (Payload payload = JsonPayloadUtil.toPayload(
								new CreateResponse(getContext().getObjectTypeId(), path, Constants.UPDATE_SUCCESS_MSG))){
							ResponseUtil.addSuccess(response, data, StringUtil.toString(statusCode), payload);

						}
					} else {
						CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
					}
				} else {
					CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.UPDATE_INPUT_MSG);
				}
			} finally {
				IOUtil.closeQuietly(httpResponse);
			}
		} else {
			CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.PATH_MSG);
		}
	}


	/**
	 * Invoke update.
	 *
	 * @param path the path
	 * @param action the action
	 * @param size the size
	 * @param buf the buf
	 * @return the closeable http response
	 * @throws URISyntaxException the URI syntax exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private CloseableHttpResponse invokeUpdate(String path, String action, long size, byte[] buf) throws URISyntaxException, IOException {
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			URIBuilder builder2 = new URIBuilder(path);
			builder2.setParameter(Constants.ACTION, action);
			builder2.setParameter(Constants.POSITION, String.valueOf(size));
			HttpPatch httpPatch = new HttpPatch(builder2.build().toString());
			httpPatch.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
			httpPatch.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());
			httpPatch.addHeader(Constants.CONTENT, Constants.OCTET_STREAM);
			if ("append".equals(action)) {
				httpPatch.setEntity(new ByteArrayEntity(buf));
			}
			httpResponse = httpClient.execute(httpPatch);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_ACCEPTED && statusCode != HttpStatus.SC_OK) {
				throw new ConnectorException(httpResponse.getStatusLine().getReasonPhrase());
			}
		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
		return httpResponse;
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	@Override
	public AzureDataLakeConnection getConnection() {
		return (AzureDataLakeConnection) super.getConnection();
	}
}