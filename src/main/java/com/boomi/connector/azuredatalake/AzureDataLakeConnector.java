// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.SchemaBuilderUtil;
import com.boomi.connector.util.BaseConnector;

/**
 * The Class AzureDataLakeConnector.
 *
 * 
 */
public class AzureDataLakeConnector extends BaseConnector {

	/**
	 * Creates the browser.
	 *
	 * @param context the context
	 * @return the browser
	 */
	@Override
	public Browser createBrowser(BrowseContext context) {
		return new AzureDataLakeBrowser(createConnection(context));
	}

	/**
	 * Creates the execute operation.
	 *
	 * @param context the context
	 * @return the operation
	 */
	@Override
	protected Operation createExecuteOperation(OperationContext context) {
		String opsType = context.getCustomOperationType();
		if (Constants.LIST.equals(opsType)) {
			return new AzureDataLakeGetOperation(createConnection(context));
		}
		if (Constants.DELETE.equals(opsType)) {
			return new AzureDataLakeDeleteOperation(createConnection(context));
		}
		if (Constants.GET.equals(opsType)) {
			return new AzureDataLakeReadOperation(createConnection(context));
		}
		return null;
	}

	/**
	 * Creates the create operation.
	 *
	 * @param context the context
	 * @return the operation
	 */

	@Override
	protected Operation createCreateOperation(OperationContext context) {
		String cookie = context.getObjectDefinitionCookie(ObjectDefinitionRole.INPUT);
		String createType = SchemaBuilderUtil.getOperationType(cookie, Constants.CREATE_TYPE);
		if (Constants.PATH.equals(createType)) {
			return new AzureDataLakeCreatePathOperation(createConnection(context));
		}
		else {
		return new AzureDataLakeCreateOperation(createConnection(context));
		}
	}

	/**
	 * Creates the update operation.
	 *
	 * @param context the context
	 * @return the operation
	 */
	@Override
	protected Operation createUpdateOperation(OperationContext context) {
		return new AzureDataLakeUpdateOperation(createConnection(context));
	}

	/**
	 * Creates the connection.
	 *
	 * @param context the context
	 * @return the azure data lake connection
	 */
	private AzureDataLakeConnection createConnection(BrowseContext context) {
		return new AzureDataLakeConnection(context);
	}
}