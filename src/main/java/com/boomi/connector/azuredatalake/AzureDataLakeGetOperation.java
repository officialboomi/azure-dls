// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.logging.Level;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import com.boomi.connector.api.BasePayload;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.CustomResponseUtil;
import com.boomi.connector.azuredatalake.utils.SchemaBuilderUtil;
import com.boomi.connector.azuredatalake.utils.AzurePayloadUtil;
import com.boomi.connector.util.SizeLimitedUpdateOperation;
import com.boomi.util.IOUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The Class AzureDataLakeGetOperation.
 */
public class AzureDataLakeGetOperation extends SizeLimitedUpdateOperation {

	private AzureDataLakeConnection conn = null;

	/**
	 * Instantiates a new azure data lake get operation.
	 *
	 * @param conn the conn
	 */
	public AzureDataLakeGetOperation(AzureDataLakeConnection conn) {
		super(conn);
		this.conn = conn;
	}

	/**
	 * Execute update.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	@Override
	protected void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {
		String cookie = getContext().getObjectDefinitionCookie(ObjectDefinitionRole.INPUT);
		String listType = SchemaBuilderUtil.getOperationType(cookie, Constants.LIST_TYPE);

		for (ObjectData data : request) {
			if (!Constants.PATH.equals(listType)) {
				listFileSystem(data, response);
			} else {
				String fileSystemName = getContext().getObjectTypeId();
				listPath(data, fileSystemName, response);
			}
		}
	}

	/**
	 * Method to invoke List Filesystem API to fetch the list of filesytem
	 *
	 * @param data the data
	 * @param response the response
	 */
	private void listFileSystem(ObjectData data, OperationResponse response) {
		CloseableHttpResponse httpResponse = null;
		InputStream is = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			long max = getContext().getOperationProperties().getLongProperty("maxResults", (long) 0);
			HttpGet httpGet = new HttpGet(buildURL(data, null, max));
			this.addHeaders(httpGet);
			httpResponse = httpClient.execute(httpGet);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				is = httpResponse.getEntity().getContent();
				this.getResponseData(is, data, statusCode, response);
				Header[] headers = httpResponse.getHeaders("x-ms-continuation");
				if (headers != null && max > 5000) {
					max = max - 5000;
					this.getNextList(headers, data, response, max);
				}
				response.finishPartialResult(data);

			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}
		} catch (IOException | URISyntaxException e) {
			CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
		} finally {
			IOUtil.closeQuietly(httpResponse, is);
		}
	}

	/**
	 * Method to invoke List Path API to fetch the list of Path in the given filesytem
	 *
	 * @param data           the data
	 * @param fileSystemName the file system name
	 * @param response       the response
	 */
	private void listPath(ObjectData data, String fileSystemName, OperationResponse response) {
		CloseableHttpResponse httpResponse = null;
		InputStream in = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			long max = getContext().getOperationProperties().getLongProperty("maxResults", (long) 0);
			HttpGet httpGet = new HttpGet(buildListPathURL(data, null, max, fileSystemName));
			this.addHeaders(httpGet);
			httpResponse = httpClient.execute(httpGet);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				in = httpResponse.getEntity().getContent();
				this.getResponseData(in, data, statusCode, response);
				response.finishPartialResult(data);
			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}

		} catch (IOException | URISyntaxException e) {
			CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
		} finally {
			IOUtil.closeQuietly(in);
		}
	}

	/**
	 * Builds the URL.
	 *
	 * @param data         the data
	 * @param continuation the continuation
	 * @param max          the max
	 * @return the string
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private String buildURL(ObjectData data, String continuation, long max) throws IOException, URISyntaxException {
		String url = null;
		try (InputStream is = data.getData()) {
			URIBuilder builder = new URIBuilder(this.conn.getUrl());
			builder.setParameter(Constants.RESOURCE, Constants.ACCOUNT);

			if (is.available() != 0) {
				JsonNode json = JSONUtil.getDefaultObjectMapper().readTree(is);
				if (json.get(Constants.PREFIX) != null) {
					builder.setParameter(Constants.PREFIX, json.get(Constants.PREFIX).textValue());
				}
			}

			if (max > 0) {
				builder.setParameter(Constants.MAX_RESULT, String.valueOf(max));
			}
			if (continuation != null) {
				builder.setParameter(Constants.CONTINUATION, continuation);
			}
			url = builder.build().toString();
		}
		return url;

	}

	/**
	 * Adds the headers.
	 *
	 * @param httpGet the http get
	 */
	private void addHeaders(HttpGet httpGet) {
		httpGet.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
		httpGet.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());
	}

	/**
	 * Method to split the response from List Filesystem/Path API by traversing the
	 * parser object and by creating individual documents from each of the object
	 * present in the array "responses".
	 *
	 * @param in         the in
	 * @param data       the data
	 * @param statusCode the status code
	 * @param response   the response
	 * @return the response data
	 */
	private void getResponseData(InputStream in, ObjectData data, int statusCode, OperationResponse response) {
		try (JsonParser jp = JSONUtil.getDefaultObjectMapper().getFactory().createParser(in)) {
			while (jp.nextToken() != JsonToken.END_OBJECT) {
				if (jp.getCurrentToken() == JsonToken.START_ARRAY) {
					while (jp.getCurrentToken() != JsonToken.END_ARRAY) {
						jp.nextToken();
						if (jp.getCurrentToken() == JsonToken.START_OBJECT) {
							try(BasePayload payload = AzurePayloadUtil.toPayload(jp)){
								ResponseUtil.addPartialSuccess(response, data, String.valueOf(statusCode), payload);	
							}
						}
					}
					break;
				}
			}

		} catch (Exception e) {
			data.getLogger().log(Level.WARNING, e.getMessage(), e);
			response.addPartialResult(data, OperationStatus.APPLICATION_ERROR, String.valueOf(statusCode),
					Constants.ERROR_PROCESSING_BODY_CONTENT, null);
		} 
	}

	/**
	 * Gets the next list.
	 * 
	 * Caling list api to fetch the next set of list path/filesystem by sending
	 * "continuation" parameter from "x-ms-continuation" header in previous list api
	 * call.This will be called if maxresults are greater than 5000 &
	 * ""x-ms-continuation" header value is not null.
	 *
	 * @param headers        the headers
	 * @param data           the data
	 * @param response		 the operation response
	 * @param max            the max
	 * @param fileSystemName the file system name
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void getNextList(Header[] headers, ObjectData data, OperationResponse response, long max) throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			for (Header header : headers) {
				if (!header.getValue().isEmpty()) {
					HttpGet httpGet = new HttpGet(buildURL(data, header.getValue(), max));
					this.addHeaders(httpGet);
					InputStream is = null;
					try {
						httpResponse = httpClient.execute(httpGet);
						int statusCode = httpResponse.getStatusLine().getStatusCode();
						if (statusCode == HttpStatus.SC_OK) {
							is = httpResponse.getEntity().getContent();
							max = max - 5000;
							this.getResponseData(is, data, statusCode, response);
							Header[] responseHeaders = httpResponse.getHeaders("x-ms-continuation");
							for (Header responseHeader : responseHeaders) {
								if (!responseHeader.getValue().isEmpty() && max > 0) {
									this.getNextList(responseHeaders, data, response, max);
								}
							}
						}
					} finally {
						IOUtil.closeQuietly(is, httpResponse);
					}
				}
			}
		}
	}

	/**
	 * Builds the list path URL.
	 *
	 * @param data           the data
	 * @param continuation   the continuation
	 * @param max            the max
	 * @param fileSystemName the file system name
	 * @return the string
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private String buildListPathURL(ObjectData data, String continuation, long max, String fileSystemName)
			throws URISyntaxException, IOException {
		String url = getConnection().getUrl() + "/" + fileSystemName;
		try (InputStream is = data.getData()) {
			URIBuilder builder = new URIBuilder(url);
			builder.setParameter(Constants.RESOURCE, Constants.FILE_SYSTEM);
			boolean recursive = getContext().getOperationProperties().getBooleanProperty("recursive", false);
			builder.setParameter(Constants.RECURSIVE, String.valueOf(recursive));

			if (is.available() != 0) {
				JsonNode json = JSONUtil.getDefaultObjectMapper().readTree(is);
				if (json.get(Constants.DIRECTORY) != null) {
					builder.setParameter(Constants.DIRECTORY, json.get(Constants.DIRECTORY).textValue());
				}
			}

			if (max > 0) {
				builder.setParameter(Constants.MAX_RESULT, String.valueOf(max));
			}
			if (continuation != null) {
				builder.setParameter(Constants.CONTINUATION, continuation);
			}
			url = builder.build().toString();
		}
		return url;
	}

	@Override
	public AzureDataLakeConnection getConnection() {
		return (AzureDataLakeConnection) super.getConnection();
	}
}
