// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

/**
 * The Class AzureDataLakeConnection.
 * 
 */
public class AzureDataLakeConnection extends BaseConnection {
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(AzureDataLakeConnection.class.getName());

	/** The context. */
	private BrowseContext context;

	/** The client id. */
	private final String clientId;

	/** The client secret. */
	private final String clientSecret;

	/** The token url. */
	private final String tokenUrl;

	/** The scope. */
	private final String scope;

	/** The access token. */
	private String accessToken;

	/** The token generation time. */
	private LocalTime tokenGenerationTime;

	/** The expires in. */
	private int expiresIn;

	/** The account name. */
	private final String accountName;

	/** The url. */
	private String url;

	/** The timevalue. */
	private String timevalue;

	/**
	 * Gets the timevalue.
	 *
	 * @return the timevalue
	 */
	public String getTimevalue() {
		return timevalue;
	}

	/**
	 * Sets the timevalue.
	 *
	 * @param timevalue the timevalue to set
	 */
	public void setTimevalue(String timevalue) {
		this.timevalue = timevalue;
	}

	/**
	 * Instantiates a new azure data lake connection.
	 *
	 * @param context the context
	 */
	public AzureDataLakeConnection(BrowseContext context) {
		super(context);
		this.context = context;
		PropertyMap connectionProperties = context.getConnectionProperties();
		this.accountName = connectionProperties.getProperty(Constants.ACCOUNT_NAME);
		this.clientId = connectionProperties.getProperty(Constants.CLIENT_ID_KEY);
		this.clientSecret = connectionProperties.getProperty(Constants.CLIENT_SECRET_KEY);
		this.scope = connectionProperties.getProperty(Constants.SCOPE_KEY);
		this.tokenUrl = connectionProperties.getProperty(Constants.TOKEN_URL);
		String endPoint = connectionProperties.getProperty(Constants.ENDPOINT_KEY);
		this.setUrl("https://" + this.accountName + ".dfs." + endPoint);
		long readTimeout = connectionProperties.getLongProperty(Constants.RESPONSE_TIMEOUT, (long) 30000);
		this.setTimevalue(String.valueOf(readTimeout));
		this.accessToken = generateAccessToken();
	}

	/**
	 * Method to make a POST call to the authentication endpoint by setting the body
	 * as x-www-form-url encoded entity with appropriate headers to have
	 * authorization granted and generate the access token.
	 * 
	 * @return an instance of {@link String}.
	 */
	public String generateAccessToken() {
		CloseableHttpResponse response = null;
		InputStream stream = null;
		JsonParser parser = null;
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(tokenUrl);
			List<NameValuePair> parameters = new ArrayList<>();
			parameters.add(new BasicNameValuePair(Constants.GRANT_TYPE_KEY, Constants.GRANT_TYPE_VALUE));
			parameters.add(new BasicNameValuePair(Constants.CLIENT_ID_KEY, clientId));
			parameters.add(new BasicNameValuePair(Constants.CLIENT_SECRET_KEY, clientSecret));
			parameters.add(new BasicNameValuePair(Constants.SCOPE_KEY, scope));
			httpPost.setEntity(new UrlEncodedFormEntity(parameters));
			httpPost.addHeader(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE_VALUE);
			response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			stream = entity.getContent();
			parser = JSONUtil.getDefaultObjectMapper().getFactory().createParser(stream);
			if (response.getStatusLine().getStatusCode() == 200) {
				this.accessToken = determineAccessToken(parser);
				this.tokenGenerationTime = LocalTime.now();
			} else {
				String errorMsg = getErrorMessage(parser, Constants.ERROR_DESCRIPTION);
				throw new ConnectorException(errorMsg);
			}
		} catch (IOException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(response);
			IOUtil.closeQuietly(stream);
			IOUtil.closeQuietly(parser);
		}
		return accessToken;
	}

	/**
	 * Utility method to traverse the response body string and determine the value
	 * of "access_token", using Jackson libraries.
	 * 
	 * @param jParser - parser formed out of the InputStream by invoking
	 *                authentication API. Type {@link JsonParser}.
	 * @return - an instance of {@link String}.
	 */
	private String determineAccessToken(JsonParser jParser) {
		String token = null;
		try {
			while (jParser.nextToken() != JsonToken.END_OBJECT) {
				String fieldName = jParser.getCurrentName();
				if (Constants.ACCESS_TOKEN_KEY.equals(fieldName)) {
					jParser.nextToken();
					token = jParser.getText();
				}
				if (Constants.EXPIRES_IN_KEY.equals(fieldName)) {
					jParser.nextToken();
					this.expiresIn = jParser.getIntValue();
				}
				if (token != null && expiresIn > 0) {
					break;
				}
			}
		} catch (IOException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(jParser);
		}
		return token;
	}

	/**
	 * Getter method to fetch the instance of {@link BrowseContext}.
	 * 
	 * @return an instance of {@link BrowseContext}.
	 */
	@Override
	public BrowseContext getContext() {
		return context;
	}

	/**
	 * Getter method to fetch the access token generated while initializing the
	 * constructor of this class.
	 * 
	 * @return an instance of {@link String}.
	 */
	public String getAccessToken() {
		if (isTokenExpired()) {
			this.accessToken = generateAccessToken();
		}
		return accessToken;
	}

	/**
	 * Test.
	 */
	public void test() {
		CloseableHttpResponse response = null;
		InputStream stream = null;
		JsonParser parser = null;
		if (accountName != null && !accountName.isEmpty()) {
			try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
				URIBuilder builder = new URIBuilder(url);
				builder.setParameter(Constants.RESOURCE, Constants.ACCOUNT);
				builder.setParameter(Constants.RESPONSE_TIMEOUT, this.timevalue);
				HttpGet httpget = new HttpGet(builder.build().toString());
				httpget.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
				httpget.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.getAccessToken());
				response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				stream = entity.getContent();
				parser = JSONUtil.getDefaultObjectMapper().getFactory().createParser(stream);
				if (response.getStatusLine().getStatusCode() != 200) {
					String errorMsg = getErrorMessage(parser, Constants.MESSAGE);
					throw new ConnectorException(errorMsg);
				}

			} catch (IOException | URISyntaxException e) {
				throw new ConnectorException(e);
			}

			finally {
				IOUtil.closeQuietly(response);
				IOUtil.closeQuietly(stream);
				IOUtil.closeQuietly(parser);
			}
		} else {
			throw new ConnectorException("Please provide Account Name details");
		}
	}

	/**
	 * Gets the error message.
	 *
	 * @param jParser   the j parser
	 * @param jsonField the json field
	 * @return the error message
	 */
	public String getErrorMessage(JsonParser jParser, String jsonField) {
		String errorMsg = null;
		try {
			while (jParser.nextToken() != JsonToken.END_OBJECT) {
				if (jsonField.equals(jParser.getCurrentName())) {
					jParser.nextToken();
					errorMsg = jParser.getText();
					break;
				}
			}
		} catch (IOException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(jParser);
		}
		return errorMsg;
	}

	/**
	 * Gets the file system.
	 *
	 * @return the file system
	 */
	public List<String> getFileSystem() {
		CloseableHttpResponse response = null;
		InputStream stream = null;
		JsonParser parser = null;
		List<String> fileSystemList = new ArrayList<String>();
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			URIBuilder builder = new URIBuilder(this.getUrl());
			builder.setParameter(Constants.RESOURCE, Constants.ACCOUNT);
			HttpGet httpget = new HttpGet(builder.build().toString());
			httpget.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
			httpget.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.getAccessToken());
			response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			stream = entity.getContent();
			parser = JSONUtil.getDefaultObjectMapper().getFactory().createParser(stream);
			if (response.getStatusLine().getStatusCode() != 200) {
				String errorMsg = getErrorMessage(parser, Constants.MESSAGE);
				throw new ConnectorException(errorMsg);
			} else {
				while (parser.nextToken() != JsonToken.END_ARRAY) {
					if ("name".equals(parser.getCurrentName())) {
						parser.nextToken();
						fileSystemList.add(parser.getText());
					}
				}
			}
		} catch (IOException | URISyntaxException e) {
			throw new ConnectorException(e);
		}

		finally {
			IOUtil.closeQuietly(response);
			IOUtil.closeQuietly(stream);
			IOUtil.closeQuietly(parser);
		}
		return fileSystemList;

	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Method to determine if the access token generated has expired or not.
	 *
	 * @return isExpired a boolean true or false.
	 */
	public boolean isTokenExpired() {
		LocalTime willExpireIn = tokenGenerationTime.plusSeconds(expiresIn);
		boolean isExpired = willExpireIn.isBefore(LocalTime.now());
		if (isExpired) {
			logger.log(Level.INFO, Constants.LOG_ACCESS_TOKEN_EXPIRY);
		}
		return isExpired;
	}
}