// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.azuredatalake.model.CreateResponse;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.CustomResponseUtil;
import com.boomi.connector.azuredatalake.utils.SchemaBuilderUtil;
import com.boomi.connector.util.SizeLimitedUpdateOperation;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The Class AzureDataLakeDeleteOperation.
 * 
 */
public class AzureDataLakeDeleteOperation extends SizeLimitedUpdateOperation {


	private AzureDataLakeConnection conn = null;

	/**
	 * Instantiates a new azure data lake delete operation.
	 *
	 * @param conn the conn
	 */
	public AzureDataLakeDeleteOperation(AzureDataLakeConnection conn) {
		super(conn);
		this.conn = conn;
	}

	/**
	 * Execute update.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	@Override
	protected void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {
		String cookie = getContext().getObjectDefinitionCookie(ObjectDefinitionRole.INPUT);
		String deleteType = SchemaBuilderUtil.getOperationType(cookie, Constants.DELETE_TYPE);

		for (ObjectData data : request) {

			if (!Constants.PATH.equals(deleteType)) {
				executeDeleteFileSystem(data, response);

			}

			else {
				executeDeletePath(data, response);

			}
		}
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	@Override
	public AzureDataLakeConnection getConnection() {
		return (AzureDataLakeConnection) super.getConnection();
	}

	/**
	 * Execute delete file system.
	 *
	 * @param data     the data
	 * @param response the response
	 */
	private void executeDeleteFileSystem(ObjectData data, OperationResponse response) {
		JsonNode json = null;
		try (InputStream is = data.getData()) {

			if (is.available() != 0) {
				json = JSONUtil.getDefaultObjectMapper().readTree(is);
				if (json.get(Constants.FILESYSTEM) == null) {
					CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.FILESYSTEM_INPUT_MSG);

				} else {
					String fileSystemName = json.get(Constants.FILESYSTEM).toString().replace("\"", "");
					deleteFileSystem(data, fileSystemName, response);
				}
			} else {
				CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.FILESYSTEM_INPUT_MSG);
			}
		} catch (IOException | URISyntaxException e) {
			CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
		}
	}

	/**
	 * Execute delete path.
	 *
	 * @param data     the data
	 * @param response the response
	 */
	private void executeDeletePath(ObjectData data, OperationResponse response) {
		JsonNode json = null;
		try (InputStream is = data.getData()) {

			if (is.available() != 0) {
				json = JSONUtil.getDefaultObjectMapper().readTree(is);
				String directoryName = null;
				StringBuilder url = new StringBuilder();
				StringBuilder path = new StringBuilder();
				if (json.get(Constants.DIRECTORY) != null)
					directoryName = json.get(Constants.DIRECTORY).asText();
				String fileName = null;
				if (json.get(Constants.FILE_NAME) != null)
					fileName = json.get(Constants.FILE_NAME).asText();
				String fileSystemName = getContext().getObjectTypeId();
				url.append(this.conn.getUrl()).append("/").append(fileSystemName);
				if (StringUtil.isEmpty(directoryName) && StringUtil.isEmpty(fileName)) {
					CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.INPUT_MSG);

				} else {
					if (!StringUtil.isEmpty(directoryName)) {
						url.append("/").append(directoryName);
						path.append(directoryName);
					}
					if (!StringUtil.isEmpty(fileName)) {
						url.append("/").append(fileName);
						path.append("/").append(fileName);
					}
					deletePath(data, url.toString(), response, path.toString());
				}

			} else {
				CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.INPUT_MSG);
			}

		} catch (IOException | URISyntaxException e) {
			CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
		}
	}

	/**
	 * Delete file system.
	 *
	 * @param data           the data
	 * @param fileSystemName the file system name
	 * @param response       the response
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void deleteFileSystem(ObjectData data, String fileSystemName, OperationResponse response)
			throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			String url = this.conn.getUrl() + "/" + fileSystemName;
			URIBuilder builder = new URIBuilder(url);
			builder.setParameter(Constants.RESOURCE, Constants.FILE_SYSTEM);
			builder.setParameter(Constants.RESPONSE_TIMEOUT, this.conn.getTimevalue());
			HttpDelete httpdelete = new HttpDelete(builder.build().toString());
			this.addHeaders(httpdelete, false);
			httpResponse = httpClient.execute(httpdelete);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_ACCEPTED) {
				try (Payload responseUtil = JsonPayloadUtil
						.toPayload(new CreateResponse(fileSystemName, "", Constants.DELETE_SUCESS_MSG))) {
					ResponseUtil.addSuccess(response, data, StringUtil.toString(statusCode), responseUtil);
				}

			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}

		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
	}

	/**
	 * Delete path.
	 *
	 * @param data     the data
	 * @param url      the url
	 * @param response the response
	 * @param path     the path
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void deletePath(ObjectData data, String url, OperationResponse response, String path)
			throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			HttpDelete httpDelete = new HttpDelete(this.buildDeletePathURL(null, url));
			this.addHeaders(httpDelete, true);
			httpResponse = httpClient.execute(httpDelete);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				Header[] headers = httpResponse.getHeaders(Constants.X_MS_CONTINUATION);
				if (headers != null) {
					this.deleteNextPaths(headers, url);
				}
				try (Payload responseUtil = JsonPayloadUtil.toPayload(
						new CreateResponse(getContext().getObjectTypeId(), path, Constants.DELETE_SUCESS_MSG))) {
					ResponseUtil.addSuccess(response, data, StringUtil.toString(statusCode), responseUtil);
				}

			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}

		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
	}

	/**
	 * Adds the headers.
	 *
	 * @param httpDelete the http delete
	 * @param flag       boolean fag
	 */
	private void addHeaders(HttpDelete httpDelete, boolean flag) {
		Map<String, String> customProperty = getContext().getOperationProperties()
				.getCustomProperties("CustomProperties");
		httpDelete.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
		httpDelete.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());

		for (Map.Entry<String, String> entry : customProperty.entrySet()) {
			if (entry.getKey().equals(Constants.IF_MODIFIED_SINCE)
					|| entry.getKey().equals(Constants.IF_UNMODIFIED_SINCE)) {
				if (entry.getValue() != null) {
					String value = this.convertDateToGMTFormat(entry.getValue());
					httpDelete.addHeader(entry.getKey(), value);
				}
			} else if (flag) {
				httpDelete.addHeader(entry.getKey(), entry.getValue());
			}
		}

	}

	/**
	 * Build delete path URL.
	 *
	 * @param continuation the continuation
	 * @param url          the url
	 * @return the string
	 * @throws URISyntaxException
	 */
	private String buildDeletePathURL(String continuation, String url) throws URISyntaxException {
		URIBuilder builder = new URIBuilder(url);
		boolean recursive = getContext().getOperationProperties().getBooleanProperty(Constants.RECURSIVE, false);
		builder.setParameter(Constants.RECURSIVE, String.valueOf(recursive));
		builder.setParameter(Constants.RESPONSE_TIMEOUT, this.conn.getTimevalue());
		if (continuation != null) {
			builder.setParameter(Constants.CONTINUATION, continuation);
		}
		url = builder.build().toString();

		return url;
	}

	/**
	 * Delete next paths.
	 *
	 * @param headers the headers
	 * @param url     the url
	 * @throws URISyntaxException
	 */
	private void deleteNextPaths(Header[] headers, String url) throws URISyntaxException {
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			for (Header header : headers) {
				if (!header.getValue().isEmpty()) {
					HttpDelete httpdelete = new HttpDelete(this.buildDeletePathURL(header.getValue(), url));
					this.addHeaders(httpdelete, true);
					try (CloseableHttpResponse httpResponse = httpClient.execute(httpdelete)) {
						int statusCode = httpResponse.getStatusLine().getStatusCode();
						if (statusCode == HttpStatus.SC_OK) {
							Header[] headers1 = httpResponse.getHeaders(Constants.X_MS_CONTINUATION);
							for (Header header2 : headers1) {
								if (!header2.getValue().isEmpty()) {
									this.deleteNextPaths(headers1, url);
								}
							}
						}
					}
				}
			}
		} catch (IOException e) {
			throw new ConnectorException(e);
		}
	}

	/**
	 * Convert date to GMT format.
	 *
	 * @param date the date
	 * @return the string
	 */
	private String convertDateToGMTFormat(String date) {
		String gmtDate = null;
		try {
			Date d = new SimpleDateFormat("MM/dd/yyyy, h:mm:ss a").parse(date);
			SimpleDateFormat f = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
			f.setTimeZone(TimeZone.getTimeZone("GMT"));
			gmtDate = f.format(d);
		} catch (ParseException e) {
			throw new ConnectorException(Constants.DATE_ERROR_MSG);
		}
		return gmtDate;
	}
}