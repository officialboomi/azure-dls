// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class CreateResponse.
 *
 */
public class CreateResponse {

	/** The file system. */
	@JsonProperty("File System")
	private String fileSystem;

	/** The path. */
	@JsonProperty("Path")
	private String path;

	/** The status. */
	@JsonProperty("Status")
	private String status;

	/**
	 * Instantiates a new creates the response.
	 *
	 * @param filesystem the filesystem
	 * @param path       the path
	 * @param status     the status
	 */
	public CreateResponse(String filesystem, String path, String status) {
		this.fileSystem = filesystem;
		this.path = path;
		this.status = status;
	}

	/**
	 * Gets the file system.
	 *
	 * @return the fileSystem
	 */
	public String getFileSystem() {
		return fileSystem;
	}

	/**
	 * Sets the file system.
	 *
	 * @param fileSystem the fileSystem to set
	 */
	public void setFileSystem(String fileSystem) {
		this.fileSystem = fileSystem;
	}

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
