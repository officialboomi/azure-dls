// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake.model;

/**
 * The Class ErrorResponse.
 *
 * 
 */
public class ErrorResponse {

	/** The error code. */
	private int errorCode;

	/** The error message. */
	private String errorMessage;

	/**
	 * Instantiates a new error response.
	 *
	 * @param errorCode    the error code
	 * @param errorMessage the error message
	 */
	public ErrorResponse(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
