// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class ListPathResponse.
 */
public class ListPathResponse {

	/** The content length. */
	@JsonProperty("contentLength")
	private String contentLength;

	/** The creation time. */
	@JsonProperty("creationTime")
	private String creationTime;

	/** The etag. */
	@JsonProperty("etag")
	private String etag;

	/** The expiry time. */
	@JsonProperty("expiryTime")
	private String expiryTime;

	/** The group. */
	@JsonProperty("group")
	private String group;

	/** The is directory. */
	@JsonProperty("isdirectory")
	private String isDirectory;

	/** The last modified. */
	@JsonProperty("lastModified")
	private String lastModified;

	/** The name. */
	@JsonProperty("name")
	private String name;

	/** The owner. */
	@JsonProperty("owner")
	private String owner;

	/** The permissions. */
	@JsonProperty("permissions")
	private String permissions;

	/**
	 * Instantiates a new list path response.
	 *
	 * @param contentLength the content length
	 * @param creationTime  the creation time
	 * @param etag          the etag
	 * @param expiryTime    the expiry time
	 * @param group         the group
	 * @param isDirectory   the is directory
	 * @param lastModified  the last modified
	 * @param name          the name
	 * @param owner         the owner
	 * @param permissions   the permissions
	 */
	public ListPathResponse(String contentLength, String creationTime, String etag, String expiryTime, String group,
			String isDirectory, String lastModified, String name, String owner, String permissions) {
		super();
		this.contentLength = contentLength;
		this.creationTime = creationTime;
		this.etag = etag;
		this.expiryTime = expiryTime;
		this.group = group;
		this.isDirectory = isDirectory;
		this.lastModified = lastModified;
		this.name = name;
		this.owner = owner;
		this.permissions = permissions;
	}

	/**
	 * Gets the checks if is directory.
	 *
	 * @return the checks if is directory
	 */
	public String getIsDirectory() {
		return isDirectory;
	}

	/**
	 * Sets the checks if is directory.
	 *
	 * @param isDirectory the new checks if is directory
	 */
	public void setIsDirectory(String isDirectory) {
		this.isDirectory = isDirectory;
	}

	/**
	 * Gets the content length.
	 *
	 * @return the content length
	 */
	public String getContentLength() {
		return contentLength;
	}

	/**
	 * Sets the content length.
	 *
	 * @param contentLength the new content length
	 */
	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}

	/**
	 * Gets the creation time.
	 *
	 * @return the creation time
	 */
	public String getCreationTime() {
		return creationTime;
	}

	/**
	 * Sets the creation time.
	 *
	 * @param creationTime the new creation time
	 */
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Gets the etag.
	 *
	 * @return the etag
	 */
	public String getEtag() {
		return etag;
	}

	/**
	 * Sets the etag.
	 *
	 * @param etag the new etag
	 */
	public void setEtag(String etag) {
		this.etag = etag;
	}

	/**
	 * Gets the expiry time.
	 *
	 * @return the expiry time
	 */
	public String getExpiryTime() {
		return expiryTime;
	}

	/**
	 * Sets the expiry time.
	 *
	 * @param expiryTime the new expiry time
	 */
	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}

	/**
	 * Gets the group.
	 *
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Sets the group.
	 *
	 * @param group the new group
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * Gets the last modified.
	 *
	 * @return the last modified
	 */
	public String getLastModified() {
		return lastModified;
	}

	/**
	 * Sets the last modified.
	 *
	 * @param lastModified the new last modified
	 */
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * Gets the permissions.
	 *
	 * @return the permissions
	 */
	public String getPermissions() {
		return permissions;
	}

	/**
	 * Sets the permissions.
	 *
	 * @param permissions the new permissions
	 */
	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

}
