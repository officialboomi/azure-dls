// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class ListFileSystem.
 */
public class ListFileSystem {

	/** The default encryption scope. */
	@JsonProperty("DefaultEncryptionScope")
	private String defaultEncryptionScope;

	/** The deny encryption scope override. */
	@JsonProperty("DenyEncryptionScopeOverride")
	private String denyEncryptionScopeOverride;

	/** The etag. */
	@JsonProperty("etag")
	private String etag;

	/** The last modified. */
	@JsonProperty("lastModified")
	private String lastModified;

	/** The name. */
	@JsonProperty("name")
	private String name;

	/**
	 * Instantiates a new list file system.
	 *
	 * @param defaultEncryptionScope      the default encryption scope
	 * @param denyEncryptionScopeOverride the deny encryption scope override
	 * @param etag                        the etag
	 * @param lastModified                the last modified
	 * @param name                        the name
	 */
	public ListFileSystem(String defaultEncryptionScope, String denyEncryptionScopeOverride, String etag,
			String lastModified, String name) {
		this.defaultEncryptionScope = defaultEncryptionScope;
		this.denyEncryptionScopeOverride = denyEncryptionScopeOverride;
		this.etag = etag;
		this.lastModified = lastModified;
		this.name = name;
	}

	/**
	 * Gets the default encryption scope.
	 *
	 * @return the default encryption scope
	 */
	public String getDefaultEncryptionScope() {
		return defaultEncryptionScope;
	}

	/**
	 * Sets the default encryption scope.
	 *
	 * @param defaultEncryptionScope the new default encryption scope
	 */
	public void setDefaultEncryptionScope(String defaultEncryptionScope) {
		this.defaultEncryptionScope = defaultEncryptionScope;
	}

	/**
	 * Gets the deny encryption scope override.
	 *
	 * @return the deny encryption scope override
	 */
	public String getDenyEncryptionScopeOverride() {
		return denyEncryptionScopeOverride;
	}

	/**
	 * Sets the deny encryption scope override.
	 *
	 * @param denyEncryptionScopeOverride the new deny encryption scope override
	 */
	public void setDenyEncryptionScopeOverride(String denyEncryptionScopeOverride) {
		this.denyEncryptionScopeOverride = denyEncryptionScopeOverride;
	}

	/**
	 * Gets the etag.
	 *
	 * @return the etag
	 */
	public String getEtag() {
		return etag;
	}

	/**
	 * Sets the etag.
	 *
	 * @param etag the new etag
	 */
	public void setEtag(String etag) {
		this.etag = etag;
	}

	/**
	 * Gets the last modified.
	 *
	 * @return the last modified
	 */
	public String getLastModified() {
		return lastModified;
	}

	/**
	 * Sets the last modified.
	 *
	 * @param lastModified the new last modified
	 */
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

}
