// Copyright (c) 2022 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.CustomResponseUtil;
import com.boomi.connector.util.SizeLimitedUpdateOperation;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The Class AzureDataLakeReadOperation.
 */
public class AzureDataLakeReadOperation extends SizeLimitedUpdateOperation {

	private AzureDataLakeConnection conn = null;

	/**
	 * Instantiates a new azure data lake read operation.
	 *
	 * @param conn the conn
	 */
	public AzureDataLakeReadOperation(AzureDataLakeConnection conn) {
		super(conn);
		this.conn = conn;
	}

	/**
	 * Execute update.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	@Override
	protected void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {
		for (ObjectData data : request) {

			try (InputStream is = data.getData()) {

				if (is.available() != 0) {
					JsonNode json = JSONUtil.getDefaultObjectMapper().readTree(is);
					String directoryName = null, fileName = null;
					StringBuilder url = new StringBuilder();
					String fileSystemName = getContext().getObjectTypeId();
					if (json.get(Constants.DIRECTORY) != null)
						directoryName = json.get(Constants.DIRECTORY).asText();
					if (json.get(Constants.FILE_NAME) != null)
						fileName = json.get(Constants.FILE_NAME).asText();
					url.append(this.conn.getUrl()).append("/").append(fileSystemName);
					if (!StringUtil.isEmpty(directoryName)) {
						url.append("/").append(directoryName);
					}
					if (!StringUtil.isEmpty(fileName)) {
						url.append("/").append(fileName);
						executeReadFile(data, url.toString(), response);
					} else {
						CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.FILE_INPUT_MSG);
					}
				} else {
					CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.FILE_INPUT_MSG);
				}
			} catch (IOException | URISyntaxException e) {
				CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
			}
		}
	}

	/**
	 * Gets the path.
	 *
	 * @param data     the data
	 * @param url      the url
	 * @param response the response
	 * @return the path
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private void executeReadFile(ObjectData data, String url, OperationResponse response)
			throws URISyntaxException, IOException {
		CloseableHttpResponse httpResponse = null;
		InputStream inputStream = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

			URIBuilder builder = new URIBuilder(url);
			builder.setParameter(Constants.RESPONSE_TIMEOUT, this.conn.getTimevalue());
			HttpGet httpget = new HttpGet(builder.build().toString());
			this.addHeaders(httpget);
			httpResponse = httpClient.execute(httpget);
			inputStream = httpResponse.getEntity().getContent();
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				try (Payload responseUtil = ResponseUtil.toPayload(inputStream)) {
					ResponseUtil.addSuccess(response, data, StringUtil.toString(statusCode), responseUtil);

				}
			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}

		} finally {
			IOUtil.closeQuietly(httpResponse, inputStream);
		}
	}

	/**
	 * Adds the headers.
	 *
	 * @param httpget the httpget
	 */
	public void addHeaders(HttpGet httpget) {
		httpget.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
		httpget.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	@Override
	public AzureDataLakeConnection getConnection() {
		return (AzureDataLakeConnection) super.getConnection();
	}

}
