// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.azuredatalake.model.CreateResponse;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.CustomResponseUtil;
import com.boomi.connector.util.SizeLimitedUpdateOperation;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The Class AzureDataLakeCreateOperation
 * 
 */
public class AzureDataLakeCreateOperation extends SizeLimitedUpdateOperation {

	private AzureDataLakeConnection conn = null;

	/**
	 * Instantiates a new azure data lake create operation.
	 *
	 * @param conn the conn
	 */
	public AzureDataLakeCreateOperation(AzureDataLakeConnection conn) {
		super(conn);
		this.conn = conn;
	}

	/**
	 * Execute update.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	protected void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {

		JsonNode json = null;
		for (ObjectData data : request) {
			try (InputStream is = data.getData()) {
				if (is.available() != 0) {
					json = JSONUtil.getDefaultObjectMapper().readTree(is);
					if (json.get(Constants.FILESYSTEM) == null) {
						CustomResponseUtil.writeApplicationErrorResponse(data, response,
								Constants.FILESYSTEM_INPUT_MSG);

					} else {
						String fileSystemName = json.get(Constants.FILESYSTEM).toString().replace("\"", "");
						createFileSystem(data, fileSystemName, response);
					}
				} else {
					CustomResponseUtil.writeApplicationErrorResponse(data, response, Constants.FILESYSTEM_INPUT_MSG);
				}
			} catch (IOException | URISyntaxException e) {
				CustomResponseUtil.writeExceptionErrorResponse(e, response, data);
			} catch (Exception e) {
				ResponseUtil.addExceptionFailure(response, data, e);
			}

		}
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	@Override
	public AzureDataLakeConnection getConnection() {
		return (AzureDataLakeConnection) super.getConnection();
	}

	/**
	 * Creates the file system.
	 *
	 * @param data           the data
	 * @param fileSystemName the file system name
	 * @param response       the response
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public void createFileSystem(ObjectData data, String fileSystemName, OperationResponse response)
			throws IOException, URISyntaxException {
		CloseableHttpResponse httpResponse = null;
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			String url = this.conn.getUrl() + "/" + fileSystemName;
			URIBuilder builder = new URIBuilder(url);
			builder.setParameter(Constants.RESOURCE, Constants.FILE_SYSTEM);
			HttpPut httpPut = new HttpPut(builder.build().toString());
			httpPut.addHeader(Constants.X_MS_VERSION, Constants.X_MS_VERSION_ID);
			httpPut.addHeader(Constants.AUTHORIZATION, Constants.BEARER + " " + this.conn.getAccessToken());
			httpResponse = httpClient.execute(httpPut);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_CREATED) {
				try (Payload payload = JsonPayloadUtil
						.toPayload(new CreateResponse(fileSystemName, "", Constants.CREATE_SUCCESS_MSG))) {
					ResponseUtil.addSuccess(response, data, StringUtil.toString(statusCode), payload);
				}
			} else {
				CustomResponseUtil.writeHttpErrorResponse(data, response, httpResponse);
			}

		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
	}

}
