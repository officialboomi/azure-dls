// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.azuredatalake;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONObject;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.azuredatalake.utils.SchemaBuilderUtil;
import com.boomi.connector.util.BaseBrowser;

/**
 * The Class AzureDataLakeBrowser
 * 
 */
public class AzureDataLakeBrowser extends BaseBrowser implements ConnectionTester {

	/**
	 * Instantiates a new azure data lake browser.
	 *
	 * @param conn the conn
	 */
	public AzureDataLakeBrowser(AzureDataLakeConnection conn) {
		super(conn);
	}

	/**
	 * Gets the object definitions.
	 *
	 * @param objectTypeId the object type id
	 * @param roles        the roles
	 * @return the object definitions
	 */
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
		ObjectDefinitions definitions = new ObjectDefinitions();
		OperationType operation = getContext().getOperationType();
		switch (operation) {
		case CREATE:
			for (ObjectDefinitionRole role : roles) {
				ObjectDefinition objDef = new ObjectDefinition();
				String jsonSchema = null;
				switch (role) {
				case OUTPUT:
					jsonSchema = SchemaBuilderUtil.getJsonSchema(null, operation.name());
					definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
					break;
				case INPUT:
					String createType = getContext().getOperationProperties().getProperty(Constants.CREATE_TYPE, "");
					if (Constants.PATH.equals(createType)) {
						definitions = this.getUnstructuredSchema(objDef, definitions);
					} else {
						jsonSchema = SchemaBuilderUtil.getInputJsonSchema(operation.name(), createType);
						definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
					}
					JSONObject jsonCookie = new JSONObject();
					jsonCookie.put(Constants.CREATE_TYPE, createType);
					objDef.withCookie(jsonCookie.toString());
					break;
				default:
					break;

				}
			}
			break;
		case EXECUTE:
			String opsType = getContext().getCustomOperationType();

			for (ObjectDefinitionRole role : roles) {
				ObjectDefinition objDef = new ObjectDefinition();
				String jsonSchema = null;
				switch (role) {
				case OUTPUT:
					if (Constants.LIST.equals(opsType)) {
						String type = getContext().getOperationProperties().getProperty(Constants.LIST_TYPE, "");
						jsonSchema = SchemaBuilderUtil.getJsonSchema(type, opsType);
						definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
					}
					if (Constants.DELETE.equals(opsType)) {
						jsonSchema = SchemaBuilderUtil.getJsonSchema(null, opsType);
						definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
					}
					break;
				case INPUT:
					if (Constants.LIST.equals(opsType)) {
						String listType = getContext().getOperationProperties().getProperty(Constants.LIST_TYPE, "");
						jsonSchema = SchemaBuilderUtil.getInputJsonSchema(opsType, listType);
						definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
						JSONObject jsonCookie = new JSONObject();
						jsonCookie.put(Constants.LIST_TYPE, listType);
						objDef.withCookie(jsonCookie.toString());
					}
					if (Constants.DELETE.equals(opsType)) {
						String deleteType = getContext().getOperationProperties().getProperty(Constants.DELETE_TYPE,
								"");
						jsonSchema = SchemaBuilderUtil.getInputJsonSchema(opsType, deleteType);
						definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
						JSONObject jsonCookie = new JSONObject();
						jsonCookie.put(Constants.DELETE_TYPE, deleteType);
						objDef.withCookie(jsonCookie.toString());
					}
					if (Constants.GET.equals(opsType)) {
						jsonSchema = SchemaBuilderUtil.getInputJsonSchema(opsType, Constants.PATH);
						definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
					}
					break;
				default:
					break;

				}
			}
			break;
		case UPDATE:
			for (ObjectDefinitionRole role : roles) {
				ObjectDefinition objDef = new ObjectDefinition();
				String jsonSchema = null;
				switch (role) {
				case OUTPUT:
					jsonSchema = SchemaBuilderUtil.getJsonSchema(null, operation.name());
					definitions = this.getJsonStructure(jsonSchema, objDef, definitions);
					break;
				case INPUT:
					definitions = this.getUnstructuredSchema(objDef, definitions);
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
		return definitions;
	}

	/**
	 * Gets the object types.
	 *
	 * @return the object types
	 */
	@Override
	public ObjectTypes getObjectTypes() {
		ObjectTypes objectTypes = new ObjectTypes();
		switch (getContext().getOperationType()) {

		case CREATE:
			String createType = getContext().getOperationProperties().getProperty(Constants.CREATE_TYPE, null);
			if (Constants.PATH.equals(createType)) {
				objectTypes.withTypes(getPathOptObjectTypes());
			}
			else {
				objectTypes.withTypes(getOperationObjectTypes(createType));
			}
			break;

		case EXECUTE:
			String opsType = getContext().getCustomOperationType();
			if (Constants.LIST.equals(opsType)) {
				String getType = getContext().getOperationProperties().getProperty(Constants.LIST_TYPE, null);
				if (Constants.PATH.equals(getType)) {
					objectTypes.withTypes(getPathOptObjectTypes());
				}
				else {
					objectTypes.withTypes(getOperationObjectTypes(getType));
				}
				
			}
			if (Constants.DELETE.equals(opsType)) {
				String deleteType = getContext().getOperationProperties().getProperty(Constants.DELETE_TYPE, null);
				if (Constants.PATH.equals(deleteType)) {
					objectTypes.withTypes(getPathOptObjectTypes());
				}
				else {
				objectTypes.withTypes(getOperationObjectTypes(deleteType));
				}
			}
			if (Constants.GET.equals(opsType)) {
				objectTypes.withTypes(getPathOptObjectTypes());

			}
			break;

		case UPDATE:
			objectTypes.withTypes(getPathOptObjectTypes());
			break;

		default:
			break;

		}
		return objectTypes;
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	@Override
	public AzureDataLakeConnection getConnection() {
		return (AzureDataLakeConnection) super.getConnection();
	}

	/**
	 * Test connection.
	 */
	@Override
	public void testConnection() {
		getConnection().test();
	}

	/**
	 * Gets the creates the object types.
	 *
	 * @param operationType the opearation type
	 * @return the creates the object types
	 */
	private List<ObjectType> getOperationObjectTypes(String operationType) {
		List<ObjectType> types = new ArrayList<>();
		if ("FileSystem".equalsIgnoreCase(operationType)) {
			ObjectType objectType = new ObjectType();
			objectType.setId(Constants.FILE_SYSTEM);
			objectType.setLabel("FileSystem");
			types.add(objectType);
		}
		return types;

	}

	/**
	 * Gets the unstructured schema.
	 *
	 * @param objdef  the objdef
	 * @param objdefs the objdefs
	 * @return the unstructured schema
	 */
	private ObjectDefinitions getUnstructuredSchema(ObjectDefinition objdef, ObjectDefinitions objdefs) {
		objdef.setElementName("");
		objdef.setOutputType(ContentType.BINARY);
		objdef.setInputType(ContentType.NONE);

		objdefs.getDefinitions().add(objdef);
		return objdefs;
	}

	/**
	 * This method will take {@link ObjectDefinition} and jsonSchema and it will
	 * build the structured Schema for request and response profile.
	 *
	 * @param jsonSchema  the json schema
	 * @param objDef      the ObjectDefinition
	 * @param definitions the ObjectDefinitions
	 * @return ObjectDefinitions
	 */
	private ObjectDefinitions getJsonStructure(String jsonSchema, ObjectDefinition objDef,
			ObjectDefinitions definitions) {
		objDef.setElementName("");
		objDef.setJsonSchema(jsonSchema);
		objDef.setOutputType(ContentType.JSON);
		objDef.setInputType(ContentType.JSON);
		definitions.getDefinitions().add(objDef);
		return definitions;
	}
	/**
	 * Gets the creates the object types.
	 *
	 * @return the creates the object types
	 */
	private List<ObjectType> getPathOptObjectTypes() {
		List<ObjectType> types = new ArrayList<>();
			for (String filesystemName : getConnection().getFileSystem()) {
				ObjectType objectType = new ObjectType();
				objectType.setId(filesystemName);
				objectType.setLabel(filesystemName);
				types.add(objectType);
			}
		return types;

	}
}