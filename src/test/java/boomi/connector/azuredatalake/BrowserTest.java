package boomi.connector.azuredatalake;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.azuredatalake.AzureDataLakeConnector;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.testutil.ConnectorTestContext;

public class BrowserTest extends ConnectorTestContext{
	
	private static final Logger logger = Logger.getLogger(BrowserTest.class.getName());
	
	 public BrowserTest() {
		Properties props = new Properties();
		try (FileInputStream file = new FileInputStream("src/test/resources/adls.properties");){
			props.load(file);
		} catch (FileNotFoundException e) {
			logger.log(Level.INFO, "NoFile", e);
		} catch (IOException e) {
			logger.log(Level.INFO, "Input not proper", e);
		}
		Map<String, String> customProperty = new HashMap<String, String>();
		
		addConnectionProperty("storage_account", props.getProperty("account"));
		addConnectionProperty("client_id", System.getProperty("ClienId"));
		addConnectionProperty("client_secret", System.getProperty("ClientSecret"));
		addConnectionProperty("token_url", props.getProperty("url"));
		addConnectionProperty("scope", props.getProperty("scope"));
		addConnectionProperty("timeout", 30000L);
		addConnectionProperty("endpoint", props.getProperty("endpoint"));
		addConnectionProperty("scope", props.getProperty("scope"));
		
		
		long max=10;
		addOperationProperty("maxResults", max);
		addOperationProperty("recursive", true);
		addOperationProperty("CustomProperties", customProperty);
		addOperationProperty("actionIfFileExists","OVERWRITE");
		
		
	}
	 public BrowserTest(OperationType operationType, String customOP,String subOP,String objType) {
		  this();
	       setOperationType(operationType);
	       setOperationCustomType(customOP);
			setObjectTypeId(objType);
			JSONObject jsonCookie = new JSONObject();
			jsonCookie.put(customOP, subOP);
			addCookie(ObjectDefinitionRole.INPUT, jsonCookie.toString());
			addOperationProperty(customOP, subOP);
	 }
	 public BrowserTest(String customOP,String subOP,String objType) {
		 this();
		  setOperationType(OperationType.EXECUTE);
		 setOperationCustomType(customOP);
		 addOperationProperty(subOP, objType);
	 }
	@Override
	protected Class<? extends Connector> getConnectorClass() {
		return AzureDataLakeConnector.class;
	}

}
