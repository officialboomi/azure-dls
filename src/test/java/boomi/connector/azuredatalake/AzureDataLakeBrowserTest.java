package boomi.connector.azuredatalake;

import java.util.Arrays;

import org.junit.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.azuredatalake.AzureDataLakeBrowser;
import com.boomi.connector.azuredatalake.AzureDataLakeConnection;
import com.boomi.connector.azuredatalake.utils.Constants;

public class AzureDataLakeBrowserTest {
	private BrowserTest context = new BrowserTest(OperationType.CREATE,Constants.CREATE_TYPE,"FileSystem","FileSystem");
	AzureDataLakeConnection con = new AzureDataLakeConnection(context);
	AzureDataLakeBrowser browser = new AzureDataLakeBrowser(con);
	private static final String OBJECT_TYPE_ID = "FileSystem";
	
	@Test
	public void testConnection() {
		browser.testConnection();
	}

	@Test
	public void testgetObjectDefinitions_CREATE() {
		ObjectTypes objTypes = browser.getObjectTypes();
		ObjectDefinitions objDefinitions = browser.getObjectDefinitions(OBJECT_TYPE_ID,
				Arrays.asList(ObjectDefinitionRole.INPUT, ObjectDefinitionRole.OUTPUT));
	}

	
	  @Test public void testgetObjectDefinitions_LIST() { 
		  context =  new BrowserTest(Constants.LIST,Constants.LIST_TYPE,"Path");
		  con = new AzureDataLakeConnection(context);
		  browser = new AzureDataLakeBrowser(con);
		  ObjectTypes objTypes = browser.getObjectTypes();
	  ObjectDefinitions objDefinitions = browser.getObjectDefinitions(OBJECT_TYPE_ID,
	  Arrays.asList(ObjectDefinitionRole.INPUT, ObjectDefinitionRole.OUTPUT)); 
	  }
	  
	  @Test public void testgetObjectDefinitions_DELETE() { 
		  context =  new BrowserTest(Constants.DELETE,Constants.DELETE_TYPE,"Path");
		  con = new AzureDataLakeConnection(context);
		  browser = new AzureDataLakeBrowser(con);
		  ObjectTypes objTypes = browser.getObjectTypes();
	  ObjectDefinitions objDefinitions = browser.getObjectDefinitions(OBJECT_TYPE_ID,
	  Arrays.asList(ObjectDefinitionRole.INPUT, ObjectDefinitionRole.OUTPUT)); 
	  }
	  
	  @Test public void testgetObjectDefinitions_GET() { 
		  context =  new BrowserTest(OperationType.EXECUTE,Constants.GET,"Path","ayushitest");
		  con = new AzureDataLakeConnection(context);
		  browser = new AzureDataLakeBrowser(con);
		  ObjectTypes objTypes = browser.getObjectTypes();
	  ObjectDefinitions objDefinitions = browser.getObjectDefinitions(OBJECT_TYPE_ID,
	  Arrays.asList(ObjectDefinitionRole.INPUT, ObjectDefinitionRole.OUTPUT)); 
	  }
	  
	  @Test public void testgetObjectDefinitions_UPDATE() { 
		  context =  new BrowserTest(OperationType.UPDATE,"","","");
		  con = new AzureDataLakeConnection(context);
		  browser = new AzureDataLakeBrowser(con);
		  ObjectTypes objTypes = browser.getObjectTypes();
	  ObjectDefinitions objDefinitions = browser.getObjectDefinitions(OBJECT_TYPE_ID,
	  Arrays.asList(ObjectDefinitionRole.INPUT, ObjectDefinitionRole.OUTPUT)); 
	  }
	
}
