package boomi.connector.azuredatalake;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.azuredatalake.AzureDataLakeConnection;
import com.boomi.connector.azuredatalake.AzureDataLakeCreateOperation;
import com.boomi.connector.azuredatalake.utils.Constants;
import com.boomi.connector.testutil.SimpleTrackedData;

public class CreatePathOperationTest {
	
	private BrowserTest context = new BrowserTest(OperationType.CREATE,Constants.CREATE_TYPE,"Path","ayushitest");
	AzureDataLakeConnection con = new AzureDataLakeConnection(context);
	AzureDataLakeCreateOperation ops = new AzureDataLakeCreateOperation(con);
	private UpdateRequest request = mock(UpdateRequest.class);
	private OperationResponse response = mock(OperationResponse.class);
	private Logger logger = mock(Logger.class);

public static final String input = "Hello Boomi World";
	
	private Map<String, String> dynamicProps = new HashMap<>();
	
	@Before
	public void init() {
		when(response.getLogger()).thenReturn(logger);
	}
	@Test
	public void testexecuteCreateOperation() throws IOException {
		InputStream result = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		dynamicProps.put("targetDir", "");
		dynamicProps.put("filename", "first.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, result,null,dynamicProps);
		Iterator<ObjectData> objDataItr = Mockito.mock(Iterator.class);
		when(request.iterator()).thenReturn(objDataItr);
		when(objDataItr.hasNext()).thenReturn(true, false);
		when(objDataItr.next()).thenReturn(trackedData);
		when(response.getLogger()).thenReturn(Mockito.mock(Logger.class));
		
		ops.execute(request, response);
		assertTrue(true);
		result.close();
	}

	
	@Test
	public void testexecuteCreateOperationWithNoFileName() throws IOException {
		InputStream result = new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8));
		dynamicProps.put("targetDir", "");
		dynamicProps.put("filename", "");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, result,null,dynamicProps);
		Iterator<ObjectData> objDataItr = Mockito.mock(Iterator.class);
		when(request.iterator()).thenReturn(objDataItr);
		when(objDataItr.hasNext()).thenReturn(true, false);
		when(objDataItr.next()).thenReturn(trackedData);
		when(response.getLogger()).thenReturn(Mockito.mock(Logger.class));
		
		ops.execute(request, response);
		assertTrue(true);
		result.close();
	}

}
